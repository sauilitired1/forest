package com.eclipsekingdom.fractalforest.protection;

import com.plotsquared.bukkit.util.BukkitUtil;
import com.plotsquared.core.configuration.Captions;
import com.plotsquared.core.player.PlotPlayer;
import com.plotsquared.core.plot.Plot;
import com.plotsquared.core.plot.PlotArea;
import com.plotsquared.core.util.Permissions;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class PlotSquaredProtection implements IRegionProtector {

    @Override
    public boolean isAllowed(Player player, Location location) {
        final PlotArea area = BukkitUtil.getLocation(location).getPlotArea();
        if (area == null) {
            return true;
        }
        final Plot plot = BukkitUtil.getPlot(location);
        final PlotPlayer plotPlayer = BukkitUtil.getPlayer(player);
        if (plot == null) {
            return Permissions.hasPermission(plotPlayer, Captions.PERMISSION_ADMIN_BUILD_ROAD);
        }
        if (!plot.hasOwner()) {
            return Permissions.hasPermission(plotPlayer, Captions.PERMISSION_ADMIN_BUILD_UNOWNED);
        }
        if (plot.isAdded(plotPlayer.getUUID())) {
            return true;
        }
        return Permissions.hasPermission(plotPlayer, Captions.PERMISSION_ADMIN_BUILD_OTHER);
    }

}
